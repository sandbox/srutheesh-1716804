// Converts pre.pgn4web preformaated text into textarea.pgn4web
// and includes the pgn4web board.
(function ($) {

  Drupal.behaviors.pgn4web = {

    attach: function(context) {
      // TODO: only do this if we have a correct library path!
      $('pre.pgn4web').each(function(index) {
        // Give this an id and hide it.
        var this_id = "pgn4web_" + index;
        $(this).attr("id", this_id).hide();

        // Construct pgn4web url.
        // TODO: allow users to set these settings globally and board-specific.
        var pgn4web_url = Drupal.settings.pgn4web.path;
        pgn4web_url += '/board.html?';
        pgn4web_url += 'pi=' + this_id;
        pgn4web_url += '&am=n'; // autoplayMode=none
        pgn4web_url += '&bd=s'; // buttonsDisplay=standard
        pgn4web_url += '&hd=j'; // headerDisplay=justified
        pgn4web_url += '&md=f'; // movesDisplay=figurine
        pgn4web_url += '&fh=b'; // frameHeight=board
        pgn4web_url += '&fw=p'; // frameWidth=page
        pgn4web_url += '&hl=t'; // horizontalLayout=true

        // Add an iframe.
        $(this).after("<iframe class='pgn4web' src='" + pgn4web_url + "'>" + this.value + "</iframe>");
      });
    }

    // TODO: I don't understand whether we need a "detach" here... should we
    // remove all iframe.pgn4web from the content or not ?
  };

})(jQuery);

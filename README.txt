Drupal pgn4web module
========================================================================

This module allows users to add PGN chess games to content using the
javascript pgn4web library.

Installation
------------------------------------------------------------------------

1. Copy and extract the module into sites/all/modules.

2. Download the pgn4web library from http://pgn4web.casaschi.net.

3. Extract pgn4web-2.57.zip (or later) into sites/all/libraries and
   rename the directory pgn4web-2.57 to pgn4web.

4. Enable the pgn4web module on admin/modules page.

Configuration
------------------------------------------------------------------------

1. Enable the "PGN formatter" input filter for the text formats on
   admin/config/content/formats.

2. Make sure the order and configuration of the input filters is
   correct:

   a. Following input filters should appear AFTER the "PGN formatter":

      - Convert line breaks into HTML

   b. Following input filters should appear BEFORE the "PGN formatter":

      - n/a

   c. If you use any HTML filtering or correcting input filters, such
      as "Limit allowed HTML tags" or "HTML Purifier", make sure the
      '<pre>' tag is not filtered out.

      Note that it is important that the class-attribute on the '<pre>'
      element is not filtered out as well.

      To be clear: if any filter removes or alters '<pre class="pgn4web">'
      the filter will not work!

The reason '<pre>' was chosen is that it degrades well if Javascript
is not enabled.  In this case, the PGN is show as-is which is pretty
human-readable and can be indexed by search engines.

When Javascript is enabled, it searches for all '<pre>' HTML-elements
with the predefined class 'pgn4web' and converts those elements into
playable Javascript chess boards.

Usage
------------------------------------------------------------------------

For any content with a text format with an enabled "PGN formatter" input
filter, you can insert a PGN between [pgn] and [/pgn] tags.

Example:

  This is my favorite game:

  [pgn]
  [White "..."]
  [Black "..."]

  1.e4 e5 *
  [/pgn]

Known bugs
------------------------------------------------------------------------

Only unknown bugs at the moment.

TODO list
------------------------------------------------------------------------

 * Let users configure look & behavior of pgn4web, both globally and
   board-specific.

 * Add a wysiwyg plugin to add PGN games and select starting position.

Credits
------------------------------------------------------------------------

 * pgn4web authors - http://pgn4web.casaschi.net

